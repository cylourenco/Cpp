#ifndef __POINT_HPP__
#define __POINT_HPP__

#include <iostream>
#include <sstream>

using std::cout;
using std::endl;

class Point {
    int x, y;

    public:
        Point();
        Point(int, int);
        
        int getX();
        int getY();

        void setX(int);
        void setY(int);

        std::string toString() const;

        ~Point();
};

#endif 