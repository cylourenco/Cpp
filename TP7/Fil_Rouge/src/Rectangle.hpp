#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

#include <iostream>
#include <sstream>
#include "Point.hpp"
#include "Forme.hpp"

using std::cout;
using std::endl;

class Rectangle {
    Point p; 
    Forme f;
    int ordre;

    public:
        Rectangle();
        Rectangle(Point&, Forme&);

        std::string toString() const;

        ~Rectangle();
};

#endif 