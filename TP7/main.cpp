#include <iostream>

template <typename T>
const T& maximum(const T& a,const T& b) {
    return ((a > b) ? a : b);
}

int main() {
    int result1 = maximum(5, 10);
    std::cout << "Maximum of 5 and 10 is: " << result1 << std::endl;

    double result2 = maximum(3.14, 2.71);
    std::cout << "Maximum of 3.14 and 2.71 is: " << result2 << std::endl;

    // int result3 = maximum(7, 4.5);
    // std::cout << "Maximum of 7 and 4.5 is: " << result3 << std::endl;

    int result4 = maximum<double>(7, 4.5);
    std::cout << "Maximum of 7 and 4.5 (forced) is: " << result4 << std::endl;

    return 0;
}