#ifndef __COUPLE_HPP__
#define __COUPLE_HPP__

#include <iostream>
#include "Bavarde.hpp" 

using std::cout;
using std::endl;

class Couple {
    static int compteur;
    int numero;

    Bavarde b1;
    Bavarde b2;

    public:
        Couple();
        Couple(Bavarde, Bavarde);

        ~Couple();
};

#endif