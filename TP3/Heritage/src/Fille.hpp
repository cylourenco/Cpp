#ifndef __FILLE_HPP__
#define __FILLE_HPP__

#include <iostream>
#include "Mere.hpp" 


using std::cout;
using std::endl;

class Fille : public Mere{
    static int compteur;
    int numero;

    public:
        Fille();
        Fille(std::string);

        void afficherMere();
        void afficher();

        ~Fille();
};

#endif