#include "Mere.hpp" 

int Mere::compteur = 0;

Mere::Mere(){
    numero = compteur;
    compteur++;
    cout << "Construction Mere de " << numero << endl;
}

Mere::Mere(std::string n){
    numero = compteur;
    compteur++;
    name = n;
    cout << "Construction Mere de " << numero << endl;
}

int Mere::getCompteur() {
    return compteur;
} 

std::string Mere::getName() {
    return name;
} 

void Mere::afficher()  {
    cout << "Je suis une Mere" << endl;
} 

Mere::~Mere() {
    cout << "Tais-toi Mere " << numero << endl;
}