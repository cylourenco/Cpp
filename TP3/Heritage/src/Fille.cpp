#include "Fille.hpp" 

int Fille::compteur = 0;

Fille::Fille():Mere(){
    numero = compteur;
    compteur++;
    cout << "Construction Fille de " << numero << endl;
}

Fille::Fille(std::string s):Mere(s){
    numero = compteur;
    compteur++;
    cout << "Construction Fille de " << numero << endl;
}

void Fille::afficherMere(){
    cout << "Fille: " << numero << ", Mere: " << Mere::numero << endl;
}

void Fille::afficher()  {
    cout << "Je suis une Fille" << endl;
} 

Fille::~Fille() {
    cout << "Tais-toi Fille " << numero << endl;
}