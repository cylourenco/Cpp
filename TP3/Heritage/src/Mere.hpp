#ifndef __MERE_HPP__
#define __MERE_HPP__

#include <iostream>
#include <string>

using std::cout;
using std::endl;

class Mere {
    static int compteur;

    protected:
        int numero;
        std::string name;

    public:
        Mere();
        Mere(std::string);

        int getCompteur();
        std::string getName();
        void afficher();

        ~Mere();
};

#endif
