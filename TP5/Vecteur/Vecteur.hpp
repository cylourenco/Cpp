#include <iostream>
#include <cstring>

class Vecteur {
    private:
        double* data;      // Tableau d'éléments
        int     capacity;    // Capacité du tableau
        int     size;        // Nombre d'éléments réellement contenus

    public:
        Vecteur(int initialCapacity);

        Vecteur(const Vecteur& other);

        Vecteur& operator=(const Vecteur& other);

        ~Vecteur();

        int getCapacity() const;

        int getSize() const;

        void pushBack(double value);
        
        friend std::ostream& operator<<(std::ostream& os, const Vecteur& vec);

        double& operator[](int index);

        Vecteur operator+(const Vecteur& other) const;
};