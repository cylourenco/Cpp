#include "Vecteur.hpp"

int main() {
    Vecteur vec1(5);
    vec1.pushBack(1.1);
    vec1.pushBack(2.2);
    vec1.pushBack(3.3);

    Vecteur vec2(10);
    vec2.pushBack(4.4);
    vec2.pushBack(5.5);

    std::cout << "Vec1: " << vec1 << std::endl;
    std::cout << "Vec2: " << vec2 << std::endl;

    return 0;
}
