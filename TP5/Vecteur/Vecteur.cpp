#include <iostream>
#include "Vecteur.hpp"


Vecteur::Vecteur(int initialCapacity = 10) : capacity(initialCapacity), size(0) {
    data = new double[capacity];
}

Vecteur::Vecteur(const Vecteur& other) : capacity(other.capacity), size(other.size) {
    data = new double[capacity];
    std::memcpy(data, other.data, size * sizeof(double));
}

Vecteur& Vecteur::operator=(const Vecteur& other) {
    if (this != &other) {
        delete[] data;
        capacity = other.capacity;
        size = other.size;
        data = new double[capacity];
        std::memcpy(data, other.data, size * sizeof(double));
    }
    return *this;
}

Vecteur::~Vecteur() {
    delete[] data;
}

int Vecteur::getCapacity() const {
    return capacity;
}

int Vecteur::getSize() const {
    return size;
}

void Vecteur::pushBack(double value) {
    if (size == capacity) {
        capacity *= 2;
        double* newData = new double[capacity];
        std::memcpy(newData, data, size * sizeof(double));
        delete[] data;
        data = newData;
    }

    data[size++] = value;
}

std::ostream& operator<<(std::ostream& os, const Vecteur& vec) {
    for (int i = 0; i < vec.size; ++i) {
        os << vec.data[i] << " | ";
    }
    return os;
}

double& Vecteur::operator[](int index) {
    if (index < size) {
        return data[index];
    } else {
        throw std::out_of_range("Index out of bounds");
    }
}

Vecteur Vecteur::operator+(const Vecteur& other) const {
    Vecteur result(*this);
    for (int i = 0; i < other.size; ++i) {
        result.pushBack(other.data[i]);
    }
    return result;
}
