#include "Humain.hpp"

using namespace std;

Humain::Humain(const char * n, Genre g, int a) : nom(n), genre(g), age(a) {
    cout << "Humain::Humain()" << endl;
}

const char * Humain::getNom() const {
    return nom;
}

Genre Humain::getGenre() const {
    return genre;
}

int Humain::getAge() const {
    return age;
}

void Humain::setNom(const char * n) {
    nom = n;
}

void Humain::setGenre(Genre g) {
    genre = g;
}

void Humain::setAge(int a) {
    age = a;
}

Humain::~Humain() {
    cout << "Humain::~Humain()" << endl;
}
