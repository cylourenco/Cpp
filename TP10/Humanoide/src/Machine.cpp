#include "Machine.hpp"

using namespace std;

Machine::Machine(string t, int a, int i) : type(t), autonomie(a), ifixit(i) {
    cout << "Machine::Machine()" << endl;
}

string Machine::getType() const {
    return type;
}

int Machine::getAutonomie() const {
    return autonomie;
}

int Machine::getIfixit() const {
    return ifixit;
}

void Machine::setType(string& t) {
    type = t;
}

void Machine::setAutonomie(int a) {
    autonomie = a;
}

void Machine::setIfixit(int i) {
    ifixit = i;
}

Machine::~Machine() {
    cout << "Machine::~Machine()" << endl;
}
