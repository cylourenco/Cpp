#ifndef HUMAIN_HPP
#define HUMAIN_HPP

#include <iostream>
#include <string>

enum Genre {
    HOMME,
    FEMME,
    NONBINAIRE,
    AUTRE
};

class Humain {
private:
    const char * nom;
    Genre genre;
    int age;

public:
    Humain(const char * n, Genre g, int a);
    const char * getNom() const;
    Genre getGenre() const;
    int getAge() const;

    void setNom(const char * n);
    void setGenre(Genre g);
    void setAge(int a);

    virtual ~Humain();
};

#endif // HUMAIN_HPP
