#ifndef HUMANOIDE_HPP
#define HUMANOIDE_HPP

#include "Humain.hpp"
#include "Machine.hpp"

class Humanoide : public Humain, public Machine {
public:
    Humanoide(const char * n, std::string t, Genre g, int ag);
    ~Humanoide();
};

#endif // HUMANOIDE_HPP
