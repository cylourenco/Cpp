#include "Humanoide.hpp"

using namespace std;

Humanoide::Humanoide(const char * n, string t, Genre g, int ag)
    : Humain(n, g, ag), Machine(t, 0, 3) {
    cout << "Humanoide:Humanoide()" << endl;
}

Humanoide::~Humanoide() {
    cout << "Humanoide::~Humanoide()" << endl;
}
