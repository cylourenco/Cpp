#ifndef MACHINE_HPP
#define MACHINE_HPP

#include <iostream>
#include <string>

class Machine {
private:
    std::string type;
    int autonomie;
    int ifixit;

public:
    Machine(std::string t, int a, int i);
    std::string getType() const;
    int getAutonomie() const;
    int getIfixit() const;

    void setType(std::string& t);
    void setAutonomie(int a);
    void setIfixit(int i);

    virtual ~Machine();
};

#endif // MACHINE_HPP
