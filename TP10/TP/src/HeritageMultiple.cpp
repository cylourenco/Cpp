#include <iostream>

using namespace std;

class Mere {
 public:
  Mere() {
    cout << "Mere::Mere()" << endl;
  } 
  virtual ~Mere() {
    cout << "Mere::~Mere()" << endl;
  }
};

class Pere {
 public:
  Pere() {
    cout << "Pere::Pere()" << endl;
  } 
  virtual ~Pere() {
    cout << "Pere::~Pere()" << endl;
  }
};

class Enfant : public Mere, public Pere {
 public:
   Enfant() {
      cout << "Enfant:Enfant()" << endl;

   }
   ~Enfant() {
    cout << "Enfant::~Enfant()" << endl;

  }
};

int main(int, char**) {
  Enfant * m = new Enfant();

  delete m; 

  return 0;
}