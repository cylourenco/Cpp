#ifndef __BAVARDE_HPP__
#define __BAVARDE_HPP__

#include <iostream>

using std::cout;
using std::endl;

class Bavarde {
    static int compteur;
    int x;

    public:
        Bavarde();
        Bavarde(int);
        
        void fonction(Bavarde&);
        int getX();
        void afficher();

        ~Bavarde();
};

#endif

