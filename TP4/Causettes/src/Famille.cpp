#include "Bavarde.hpp" 
#include "Famille.hpp"

Famille::Famille(){
    tab = new Bavarde[1];    
} 

Famille::Famille(int t)
{
    taille = t;

    if(taille < 1)
    {
        tab = new Bavarde[taille]; 
    } 
    else if(taille == 0)
    {
        tab = new Bavarde[1];  
    }  
    else{
        cout << "Erreur de la taille" << endl;
    } 
} 

Famille::~Famille() {
    delete[] tab; 
}   