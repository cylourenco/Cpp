#include "Bavarde.hpp" 
#include "Couple.hpp"

int Couple::compteur = 0;

Couple::Couple(){
    b1 = Bavarde();
    b2 = Bavarde();
    numero = compteur;
    compteur++;
} 

Couple::Couple(Bavarde a, Bavarde b)
// ou Couple::Couple(Bavarde& a, Bavarde& b): b1(a), b2(b)
{
    b1 = a;
    b2 = b;
    numero = compteur;
    compteur++;
} 

Couple::~Couple() {

}   