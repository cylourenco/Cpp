
#include "Bavarde.hpp"

void test1(Bavarde b) {
  std::cout << "appel de fonction avec parametre objet et copie" << std::endl;
}

void test3(Bavarde& b) {
  std::cout << "appel de fonction avec référence " << endl;
}

void test4(Bavarde *b) {
  std::cout << "appel de fonction avec un pointeur sur un objet" << endl;
}

int main(int, char**) {
    Bavarde b{};
    test1(b);
    test3(b);
    test4(&b);

   return 0;
}