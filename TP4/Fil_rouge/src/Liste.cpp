#include "Liste.hpp"

int Liste::compteur = 0;

Liste::Liste() : nb_c(0), nb_r(0) {
    for(int i = 0; i < MAX; i++) {
        cercles[i] = nullptr;
        rectangles[i] = nullptr;
    }
} 

int Liste::getCompteur() {
    return compteur;
} 

std::string Liste::toString() const {
    std::stringstream ss;
    ss << "Liste: " << nb_c << " cercles, " << nb_r << " rectangles";

    ss << "Cercles" << endl;
    for(int i = 0; i < nb_c; i++) {
        ss << cercles[i]->toString() << endl;
    }

    ss << "Rectangles" << endl;
    for(int i = 0; i < nb_c; i++) {
        ss << rectangles[i]->toString() << endl;
    }

    return ss.str();
} 

Liste::~Liste() {
    for (int i = 0; i < nb_c; ++i) {
        delete cercles[i];
    }

    for (int i = 0; i < nb_r; ++i) {
        delete rectangles[i];
    }
} 
