#include "Point.hpp"

Point::Point() : x(0), y(0){}  

Point::Point(int dx, int dy) : x(dx), y(dy) {} 

int Point::getX(){
    return x;
} 

int Point::getY(){
    return y;
} 

void Point::setX(int dx){
    x = dx;
}
        
void Point::setY(int dy){
    y = dy;
}

std::string Point::toString() const{
    std::stringstream ss;
    ss << "Point " << x << " " << y;
    return ss.str();
} 

Point::~Point(){} 