#include "Forme.hpp"

int Forme::nbFormes = 0;

Forme::Forme() : w(0), h(0){
    nbFormes++;
}  

Forme::Forme(int dw, int dh) : w(dw), h(dh) {
    nbFormes++;
} 

int Forme::getW(){
    return w;
} 

int Forme::getH(){
    return h;
} 

int Forme::getNbFormes(){
    return nbFormes;
} 

std::string Forme::toString() const{
    std::stringstream ss;
    ss << "Forme " << w << " " << h;
    return ss.str();
} 

Forme::~Forme(){} 