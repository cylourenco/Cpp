 #include "Cercle.hpp"
 #include "Point.hpp" 
#include "Forme.hpp" 

Cercle::Cercle(Point &dp, Forme &df) : p(dp), f(df) {}

std::string Cercle::toString() const{
    std::stringstream ss;
    ss << "CERCLE " << p.toString() << " " << f.toString() ;
    return ss.str();
}

Cercle::~Cercle(){

}  