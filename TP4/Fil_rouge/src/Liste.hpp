#ifndef __LISTE_HPP__
#define __LISTE_HPP__

#include <iostream>
#include <sstream>
#include "Cercle.hpp"
#include "Rectangle.hpp"  

using std::cout;
using std::endl;

const int MAX = 50;

class Liste {

    public:
        Cercle * cercles[MAX]; 
        Rectangle * rectangles[MAX]; 
        int nb_r, nb_c;
        static int compteur;

        Liste();

        int getCompteur();
        std::string toString() const;

        ~Liste(); 
};

#endif 
