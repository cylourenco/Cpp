#ifndef __CERCLE_HPP__
#define __CERCLE_HPP__

#include <iostream>
#include <sstream>
#include "Point.hpp"
#include "Forme.hpp"

using std::cout;
using std::endl;


class Cercle {
    Point p; 
    Forme f;
    int ordre;

    public:
        Cercle(Point&, Forme&);

        std::string toString() const;

        ~Cercle(); 
};

#endif 