#include "Rectangle.hpp" 
#include "Point.hpp" 
#include "Forme.hpp" 

Rectangle::Rectangle(): p(), f() {} 

Rectangle::Rectangle(Point& dp, Forme& df): p(dp), f(df) {}

std::string Rectangle::toString() const {
    std::stringstream ss;
    ss << "RECTANGLE " << p.toString() << " " << f.toString() ;
    return ss.str();
}


Rectangle::~Rectangle(){

} 