#ifndef __FORME_HPP__
#define __FORME_HPP__

#include <iostream>
#include <sstream>

using std::cout;
using std::endl;

class Forme {
    int w, h;
    static int nbFormes;

    public:
        Forme();
        Forme(int, int);
        
        int getW();
        int getH();
        static int getNbFormes();

        std::string toString() const;

        ~Forme();
};

#endif 