#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <random>
#include <functional>

class Rand_0_100 {
    std::random_device rd;
    std::mt19937 gen;

public:
    Rand_0_100() : gen(rd()) {}

    int operator()() {
        std::uniform_int_distribution<int> distribution(0, 100);
        return distribution(gen);
    }
};

int main() {
    Rand_0_100 randGen;

    // Instanciation du vecteur avec std::generate
    std::vector<int> numbers1(50);
    std::generate(numbers1.begin(), numbers1.end(), std::ref(randGen));

    // Instanciation du vecteur avec std::generate_n et back_inserter
    std::vector<int> numbers2;
    std::generate_n(std::back_inserter(numbers2), 50, std::ref(randGen));

    // Affichage des résultats
    std::cout << "Vecteur généré avec std::generate:\n";
    for (const auto& num : numbers1) {
        std::cout << num << " ";
    }

    std::cout << "\n\nVecteur généré avec std::generate_n et back_inserter:\n";
    for (const auto& num : numbers2) {
        std::cout << num << " ";
    }

    return 0;
}
