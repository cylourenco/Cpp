#include <iostream>
#include <vector>
#include <algorithm>

// Foncteur pour le tri basé sur le deuxième caractère
struct SortBySecondChar {
    bool operator()(const std::string& str1, const std::string& str2) const {
        return str1.substr(1) < str2.substr(1);
    }
};

// Foncteur pour mettre en majuscule une chaîne de caractères
struct UppercaseString {
    void operator()(std::string& str) const {
        std::transform(str.begin(), str.end(), str.begin(), ::toupper);
    }
};

int main() {
    // Test du foncteur de tri
    std::vector<std::string> strings = {"abc", "xyz", "def", "uvw"};

    std::cout << "Avant le tri : ";
    for (const auto& str : strings) {
        std::cout << str << " ";
    }

    std::sort(strings.begin(), strings.end(), SortBySecondChar());

    std::cout << "\nAprès le tri : ";
    for (const auto& str : strings) {
        std::cout << str << " ";
    }

    // Test du foncteur pour mettre en majuscule
    std::for_each(strings.begin(), strings.end(), UppercaseString());

    std::cout << "\nAprès la mise en majuscule : ";
    for (const auto& str : strings) {
        std::cout << str << " ";
    }
    std::cout << std::endl;

    return 0;
}
