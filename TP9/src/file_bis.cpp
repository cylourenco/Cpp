#include <iostream>
#include <queue>
#include <vector>
#include <string>
#include <set> 

using namespace std;

class ZZ {
    string nom;
    string prenom;
    double note;

    public:
    ZZ(const string& n, const string& p, const double nt) : nom(n), prenom(p), note(nt){} 

    // bool operator<(const ZZ& other) const {
    //     return note < other.note;
    // } 

    bool operator<(const ZZ& other) const {
        return (nom + prenom) < (other.nom + other.prenom);
    } 

    friend ostream& operator<<(ostream& os, const ZZ& zz) {
        os << zz.nom << " " << zz.prenom << " : " << zz.note << "\n";
        return os;
    } 

    double getNote() const {
        return note;
    }  
};

class CompareNote {
    public :
        bool operator()(const ZZ& zz1, const ZZ& zz2) const {
            return zz1.getNote() < zz2.getNote();
        }
}; 

using vzz = vector<ZZ>;
using vzzpt = vector<ZZ*>;

int main() {
    vzz zz;

    zz.push_back(ZZ("A", "B", 85.5));
    zz.push_back(ZZ("A", "C", 92.0));
    zz.push_back(ZZ("C", "Z", 78.3));

    priority_queue<ZZ> triAlpha;

    for(vzz::iterator it = zz.begin(); it!=zz.end(); ++it)
        triAlpha.push(*it);

    // triAlpha(zz.begin(), zz.end());

    cout << "Ordre lexico :" << endl;
    while(!triAlpha.empty()) {
        cout << triAlpha.top();
        triAlpha.pop();
    }

    priority_queue<ZZ, vector<ZZ>, CompareNote> triNotes(zz.begin(), zz.end());

    cout << "Ordre Notes inverse :" << endl;
    while(!triNotes.empty()) {
        cout << triNotes.top();
        triNotes.pop();
    }

    set<ZZ> setLexico(zz.begin(), zz.end());

    cout << "Ordre lexico avec set :" << endl;
    for (const auto& z : setLexico) {
        cout << z;
    }
    return 0;
}
