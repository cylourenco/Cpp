#include <iostream>
#include <vector>
#include <numeric> // Pour std::accumulate
#include <cmath>   // Pour std::sqrt

using namespace std;

template<typename T>
class Stats {
   vector<T> data;
   T sum;
   double moy;
   double ecart;

public:
   Stats(): data(10), sum(T()), moy(.0), ecart(.0) {}

   void push_back(const T& t) { data.push_back(t); }

   void compute() {
       sum = accumulate(data.begin(), data.end(), T());
       moy = static_cast<double>(sum) / data.size();

       double variance = 0.0;
       for (const auto& value : data) {
           variance += pow(value - moy, 2);
       }

       ecart = sqrt(variance / data.size());
   }

   void display(ostream& o = cout) const {
       o << "Sum: " << sum << endl;
       o << "Mean: " << moy << endl;
       o << "Standard Deviation: " << ecart << endl;
   }
};

int main(int, char**) {
    Stats<int> is;

    is.push_back(3);
    is.push_back(4);
    is.push_back(2);
    is.compute();
    is.display();

    return 0;
}
