#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <iterator>

using namespace std;

using mss = map<string, string>;

const string& paire(const pair<string, string>& p) {
    return p.first;
}

int main(int argc, char** argv) {
    mss annuaire;

    ifstream fichier("annuaire.txt");
    if (fichier.is_open()) {
        string nom, numero;
        while (fichier >> nom >> numero) {
            annuaire[nom] = numero;
        }
        fichier.close();
    } else {
        cerr << "Erreur : Impossible d'ouvrir le fichier annuaire.txt" << endl;
        return 1;
    }

    if (argc == 1) {
        for (const auto& entry : annuaire) {
            cout << entry.first << " : " << entry.second << endl;
        }
    } else if (argc == 2) {
        string nomRecherche = argv[1];
        auto it = annuaire.find(nomRecherche);

        if (it != annuaire.end()) {
            cout << it->first << " : " << it->second << endl;
        } else {
            cout << "Non trouvé" << endl;
        }
    } else {
        cerr << "Erreur" << endl;
        return 1;
    }

    mss annuaireInverse;
    transform(annuaire.begin(), annuaire.end(),
              inserter(annuaireInverse, annuaireInverse.begin()),
              [](const pair<string, string>& p) {
                  return make_pair(p.second, p.first);
              });

    cout << "\nAnnuaire inversé :\n";
    for (const auto& entry : annuaireInverse) {
        cout << entry.first << " : " << entry.second << endl;
    }

    return 0;
}
