#include <iostream>
#include <queue>
#include <vector>
#include <string>

using namespace std;

class ZZ {
    string nom;
    string prenom;
    double note;

    public:
    ZZ(const string& n, const string& p, const double nt) : nom(n), prenom(p), note(nt){} 

    bool operator<(const ZZ& other) const {
        return note < other.note;
    } 

    friend ostream& operator<<(ostream& os, const ZZ& zz) {
        os << zz.nom << " " << zz.prenom << " : " << zz.note << "\n";
        return os;
    } 
};

using vzz = vector<ZZ>;
using vzzpt = vector<ZZ*>;

int main() {
    vzz zz;

    zz.push_back(ZZ("Nom1", "Prenom1", 85.5));
    zz.push_back(ZZ("Nom2", "Prenom2", 92.0));
    zz.push_back(ZZ("Nom3", "Prenom3", 78.3));

    priority_queue<ZZ> tri;

    for (vzz::iterator it = zz.begin(); it != zz.end(); ++it)
        tri.push(*it);

    while (!tri.empty()) {
        cout << tri.top();
        tri.pop();
    }

    vzzpt zzpt;

    zzpt.push_back(new ZZ("Nom1", "Prenom1", 85.5));
    zzpt.push_back(new ZZ("Nom2", "Prenom2", 92.0));
    zzpt.push_back(new ZZ("Nom3", "Prenom3", 78.3));


    for (const auto& ptr : zzpt) {
        cout << *ptr;
    }

    for (const auto& ptr : zzpt) {
        delete ptr;
    }

    zzpt.clear();

    return 0;
}
