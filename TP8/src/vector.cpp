#include <algorithm>
#include <vector>
#include <iostream>
#include <iterator>
#include <stack>

int main (int, char **) {
  std::vector<int> v;  
  int input;
  
  while (std::cin >> input)
    v.push_back (input);  

  std::sort(v.begin(), v.end());

  std::copy (v.begin(), v.end(), std::ostream_iterator<int> (std::cout, "\n"));

  std::cout << "Version du tableau dynamique de taille size() avec un operator[] : ";
  for (size_t i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  std::cout << std::endl;

  std::cout << "Version avec un itérateur de début begin() et un itérateur de fin end() : ";
  for (auto i = v.begin(); i != v.end(); i++) {
    std::cout << *i << " ";
  }  
  std::cout << std::endl;

  std::cout << "Version etendue du for : ";
  for (int& e : v) {
    std::cout << e << " ";
  } 
  std::cout << std::endl;
  
  return 0;
}


