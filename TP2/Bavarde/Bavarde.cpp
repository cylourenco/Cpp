#include <iostream>

using std::cout;
using std::endl;

class Bavarde {
    int x;

    public:
        Bavarde(int);
        ~Bavarde();
        int getX();

}/*bizarre(1) creer une var globale à 1*/;  

// Bavarde globale(2); creation variable globale de 2

void fonction(Bavarde& b) {
  cout << "code de la fonction" << endl;
}

int Bavarde::getX() {
    return x;
}

Bavarde::Bavarde(int i = 0) {
    x = i;
    cout << "Construction de " << x << endl;
}

Bavarde::~Bavarde() {
    cout << "Tais-toi " << x << endl;
}

int main(int, char **) {
  Bavarde b1(3);
  Bavarde b2(4);
  Bavarde * p = new Bavarde(5);
  fonction(b1);
  delete p;
  std::cout << Bavarde(0).getX() << std::endl;
  return 0;
}