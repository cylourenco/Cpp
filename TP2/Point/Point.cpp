// Fichier Point.cpp

#include <iostream>  // Inclusion d'un fichier standard
#include "Point.hpp" // Inclusion d'un fichier du répertoire courant

int Point::compteur = 0;

Point::Point()
{
  std::cout << "Constructeur sans arg" << std::endl;
  x = y = 0;
  ++compteur;
}

Point::Point(int dx = 0, int dy = 0)
{
  std::cout << "Constructeur avec arg" << std::endl;
  setX(dx);
  setY(dy);
  ++compteur;
}

int Point::getX() {
  return x;
}

int Point::getY() {
  return y;
}

int Point::getCompteur() {
  return compteur;
}

void Point::setX(int a){
    x = a;
} 

void Point::setY(int a){
    y = a;
} 

void Point::deplacerDe(int x1, int y1){
    x += x1;
    y += y1;
} 

void Point::deplacerVers(int x1, int y1){
    setX(x1);
    setY(y1);
} 

void Point::afficher(){
  std::cout << "(" << getX() << ", " << getY() << ") : compteur=" << Point::getCompteur() << std::endl;
} 