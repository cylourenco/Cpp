// Fichier main.cpp

#include <iostream> 
#include "Point.hpp"

int main(int, char**) {

    Point p(1, 0);
    p.afficher();
    
    Point p1{};
    Point p2(2, 2);
    Point * p3 = new Point();

    p.deplacerDe(10, 10);
    p.afficher();

    p.deplacerVers(1, 1);
    p.afficher();


    (*p3).afficher();


    delete p3;

    return 0;
}