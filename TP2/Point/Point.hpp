// Fichier Point.hpp
#pragma once 

class Point {
   // par défaut, tout est privé dans une "class"  
   int x, y;


    public:
        Point();
        Point(int, int);
        int getX(); 
        int getY();
        static int getCompteur();
        void setX(int);
        void setY(int);
        void deplacerDe(int, int);
        void deplacerVers(int, int);
        void afficher();


    private:
        static int compteur;

};
