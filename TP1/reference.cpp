#include <iostream>

void fonction1(int a) {
    std::cout << &a << std::endl;
}

void fonction2(int& a) {
    std::cout << &a << std::endl;
}   

void echange_pt(int * a, int * b) {
    int c = *a;
    *a = *b;
    *b = c;
}

void echange_ref(int& a, int& b) {
    int c = a;
    a = b;
    b = c; 
}

int main(int, char**){ 
    int  a = 5;
    int &r = a;
    // si le r n'est pas initilaisé une erreure se produit

    std::cin  >> a;
    std::cout << "a : " << a << " , r : " << r << std::endl;
    std::cout << std::endl;

    int age = 45;

    std::cout << &age << std::endl;
    fonction1(age);
    fonction2(age);
    std::cout << std::endl;


    a = 3;
    int  b = a;
    int& c = a;

    std::cout  << "a : " << a << " , b : " << b << " , c : " << c << std::endl;
    b = 4;
    std::cout  << "a : " << a << " , b : " << b << " , c : " << c << std::endl;
    c = 5;
    std::cout  << "a : " << a << " , b : " << b << " , c : " << c << std::endl;
    a = 6;
    std::cout  << "a : " << a << " , b : " << b << " , c : " << c << std::endl;
    std::cout << std::endl;


    std::cout  << "Fonction d'échange par ref :" << std::endl;
    std::cout  << "a : " << a << " , b : " << b << std::endl;
    echange_ref(a, b);
    std::cout  << "a : " << a << " , b : " << b << std::endl;
    std::cout << std::endl;

    int * d = &a;
    int  * e = &b;
    std::cout  << "Fonction d'échange par pointeur :" << std::endl;
    std::cout  << "d : " << *d << " , e : " << *e << std::endl;
    echange_pt(d, e);
    std::cout  << "d : " << *d << " , e : " << *e << std::endl;
    std::cout << std::endl;

    int  f;
    int &g = f;
    std::cout << f << " " << g << std::endl; // affichage 0 0 
    std::cout << std::endl; // utiliser valgrind pour le détecter

    int        h = 1;
    const int &i = a; // i accessible en lecture et pas en écriture
    std::cout << "h : " << h << " , i : " << i << std::endl;
    // h = 2;
    // std::cout << "h : " << h << " , i : " << i << std::endl;
    // i=3;
    // std::cout << "h : " << h << " , i : " << i << std::endl;
}


