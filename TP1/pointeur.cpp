#include <iostream>
#include <string.h> 

using std::cout;
using std::endl;

int main() {
    int   a = 4;
    int * p = nullptr;

    p = &a;
    cout << "*p = " << *p << ", p = " << p << endl;
    cout << endl;

    delete p;

    const int TAILLE = 500;

    int * tab = new int[TAILLE];

    for(auto i = 0; i< TAILLE; ++i ) tab[i] = i;
    for(auto i = 0; i< TAILLE; ++i ) cout << tab[i] << endl;

    // delete tab;      
    delete [] tab;

    return 0;
}