#include <iostream>

class Exemple {
    public: // si on enlève public, il y a une erreure sur ‘void Exemple::afficher()’ is private within this context
    void afficher() {
        std::cout << "hello" << std::endl;
    }
}; // expected ‘;’ after class definition

int main(int, char **) {
  
    Exemple e;
    
    e.afficher();

    return 0;
}

