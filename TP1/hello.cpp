#include <iostream>
#include <string>

using std::cout;
using std::endl;

const int TAILLE = 10;

void afficher(int a) {
    printf("%d", a);
}

void afficher(double a) {
    printf("%lf", a);
}
 
int main(int, char **) {  // parametres muets
    std::string prenom, nom;     // type chaines de caracteres"

    std::cout << "Quel est votre prénom ?" << std::endl;
    std::cin  >> prenom;
    std::cout << "Quel est votre nom ?" << std::endl;
    std::cin  >> nom;
    

    if (prenom < nom)
    {
        std::cout << "Le prénom "<< prenom << " est plus petit avec " << prenom.length()  << std::endl;
    } 
    else
    {
        std::cout << "Le nom "<< nom << " est plus petit avec " << nom.length() << std::endl;   
    } 

    int tab[TAILLE];
  
    for (int i = 0; i < TAILLE; ++i) {
        tab[i] = i % 2;
        cout << tab[i] << " ";
    }

    cout << endl;

    afficher(1);
    afficher(2.0);

    cout << endl;

    return 0;
}