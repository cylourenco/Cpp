#ifndef __SMS
#define __SMS

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iterator>
#include <list> 
#include <forward_list>  
#include <stdexcept>
#include <cstring> 
#include <set> 

using namespace std;

class Telephone{
    string numero;
    int nbMessages;

    public :
        Telephone();
        Telephone(string c);
        Telephone(const char * c);

        int getReseau() const ;
        string getNumero() const;
        void setNumero(string s);
        int getNbMessages() const ;
        void NouveauMessage();
        void textoter(const string num, const string& msg);

        bool operator<(const Telephone& other) const;


        ~Telephone();  
} ;

/*************************RESEAU*****************************/

class Reseau {
    list<Telephone> r;

    public :
        Reseau();

        string lister() const;
        void ajouter(const string s);
        Telephone trouveTel(const string s) const;

        ~Reseau(); 
        

}; 

class MauvaisNumero : public invalid_argument{
    public:
        MauvaisNumero() : invalid_argument("oops") {} 
        MauvaisNumero(const string& msg) : invalid_argument(msg) {} 
        const char* what() const noexcept override {
            return "mauvais numero";
        }       
            
} ;

/***************************Message***********************************/

class Message {
    protected:
        string dst;
        string src;
        string date;
        const int id;
        static int cle;

    public:
        Message() : 
            src(""), dst(""), date(""), id(cle) {cle++;}
        Message(const string& d, const string& s, const string& dt) : 
        src(d), dst(s), date(dt), id(cle) {cle++;}

        virtual int getId() const 
        {
            return id;
        } 

        static int getCle() 
        {
            return cle;
        } 

        virtual string afficher() const = 0; 
} ;




/**********************SMS***********************************/

class SMS : public Message {
    string texte;

    public:
        SMS(const string& s, const string& d, const string& dt) : 
        Message(s, d, dt), texte("") {}

        void setTexte(const string& s) {
            texte = s;
        } 
        string getTexte() const {
            return texte;
        }  
        string afficher() const override {
            return texte;
        }  



        

        ~SMS() {} 
} ;

#endif