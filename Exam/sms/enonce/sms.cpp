#include "sms.hpp"

Telephone::Telephone(const char * c) : numero((string)c), nbMessages(0){} 
Telephone::Telephone(string c) : numero(c), nbMessages(0){} 
Telephone::Telephone() : numero(""), nbMessages(0){} 


string Telephone::getNumero() const
{
    return numero;
} 

void Telephone::setNumero(string s)
{
    numero = s;
} 

int Telephone::getReseau() const
{
    return 0;
} 

int Telephone::getNbMessages() const {
    return nbMessages;
}

void Telephone::NouveauMessage() {
    nbMessages +=  nbMessages;
} 

void Telephone::textoter(const string num, const string& msg)
{
    nbMessages++;
    Telephone& t = Reseau::trouveTel(num);
    t.nbMessages++;
} 


bool Telephone::operator<(const Telephone& other) const {
    return numero < other.numero;
}


Telephone::~Telephone()
{
} 

/*************************RESEAU*****************************/

Reseau::Reseau() 
{

} 

string Reseau::lister() const
{
    string s = "";
    if(!r.empty())
    {
        for(auto& ri : r)
        {
            s += ri.getNumero() + "\n";
        } 
    } 
    return s;
} 

void Reseau::ajouter(const string s)
{
    r.push_back(s); 
    r.sort();
} 

Telephone Reseau::trouveTel(const string s) const {
    for (const auto& tel : r) {
        if (tel.getNumero() == s) {
            return tel;
        }
    }
    throw invalid_argument("Invalide");
}

Reseau::~Reseau()
{
}   

/************************SMS***************************/

int Message::cle = 0;