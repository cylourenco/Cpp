#ifndef marvel__hpp
#define marvel__hpp

#include<iostream>
#include<string>
#include<stdexcept>
#include<sstream>
#include<fstream>
#include<iterator>
#include<algorithm>
#include<string>
#include<cstring>
#include<vector> 
#include<memory> 

using namespace std;

class Personne {
    
    public:
        
        enum Genre {
            HOMME,
            FEMME,
            INDETERMINE
        };    
        
        
    private:
        string nom, prenom;
        Genre genre;

    public:
        static const Personne INCONNU;
        Personne() : nom("Inconnu"), prenom("Inconnu"), genre(INDETERMINE){}
        Personne(const string p, const string n, Genre g) : 
        nom(n), prenom(p), genre(g){} 
        Personne(const string p, const string n) : 
        nom(n), prenom(p), genre(INDETERMINE){} 

        string getNom() const{
            return nom;
        } 

        string getPrenom() const{
            return prenom;
        } 

        Genre getGenre() const{
            return genre;
        } 

        void setNom(string n) {
            nom = n;
        } 

        void setPrenom(string p) {
            prenom = p;
        } 

        void setGenre(Genre g) {
            genre = g;
        } 

        void afficher(stringstream& ss) const {
            string s;
            if(genre == HOMME) s = "HOMME";
            if(genre == FEMME) s = "FEMME";
            if(genre == INDETERMINE) s = "INDETERMINE";
            ss << prenom << " " << nom << " [" << s << "]";
        } 

        friend ostream& operator<<(ostream& os, const Personne& o) {
            string s;
            if(o.genre == HOMME) s = "HOMME";
            if(o.genre == FEMME) s = "FEMME";
            if(o.genre == INDETERMINE) s = "INDETERMINE";
            os << o.prenom << " " << o.nom << " [" << s << "]";
            return os;
        }  

        bool operator==(const Personne& o) const {
            return ((nom == o.nom) && (prenom == o.prenom) && (genre == o.genre));
        } 

        ~Personne() {} 
        
};

class AnonymeException : public exception {
    public:
        AnonymeException() {} 
        const char * what() const noexcept override {
            return "identite anonyme";
        } 
};

class Capacite {
    string nom;
    int niveau;

    public:
        Capacite() : nom("aucune"), niveau(0) {}
        Capacite(string n, int ni) : nom(n), niveau(ni) {}

        string getNom() const {
            return nom;
        } 

        int getNiveau() const {
            return niveau;
        } 

        virtual void utiliser(stringstream& os)  {
            os << nom << " [" << niveau << "]";
        } 

        virtual Capacite * clone() {
            return this;
        }  

        virtual ~Capacite(){} 
};



class Materiel : public Capacite {
    public:
        Materiel(string n, int ni) : Capacite(n, ni) {}

        void actionner(stringstream& os) {
            Capacite::utiliser(os);
            os << " en action";
        } 

        void utiliser(stringstream& os) override {
            actionner(os);
        } 

        Materiel * clone() override {
            return this;
        } 

        ~Materiel() {}   
};

class Physique : public Capacite {
    public:
        Physique(string n, int ni) : Capacite(n, ni) {}

        void exercer(stringstream& os) {
            Capacite::utiliser(os);
        } 

        ~Physique() {}   
};

class Psychique : public Capacite {
    public:
        Psychique(string n, int ni) : Capacite(n, ni) {}

        void penser(stringstream& os) {
            Capacite::utiliser(os);
        } 

        ~Psychique() {}   
};

class Super {
    string nom;
    bool anonyme;
    Personne personne;
    vector<Capacite*> capacite;

    public:
        Super() : 
        nom("Inconnu"), anonyme(true), personne(Personne::INCONNU) {} 
        
        Super(string nom, Personne p) : 
        nom(nom), anonyme(true), personne(p) {} 

        string getNom() const{
            return nom;
        } 
                
        void setNom(string n) {
            nom = n;
        } 

        bool estAnonyme() const{
            return anonyme;
        } 

        Personne getIdentite() const{
            if(anonyme)  {
                throw AnonymeException();
            }  
            return personne;
        } 

        void setIdentite(Personne p) {
            personne.setPrenom(p.getPrenom());
            personne.setNom(p.getNom());
            anonyme = true;
        } 

        void enregistrer() {
            anonyme = false;
        } 

        void ajouter(Capacite * c) {
            capacite.push_back(c);
        } 
       
        int getNiveau() const{
            int somme = 0;
            for(auto& c : capacite){
                somme += c->getNiveau();
            } 
            return somme;
        } 

        ~Super() {}   

}; 

class Equipe{
    string nom;
    vector<Super*> equipe;
    static int nombre;

    public:
        Equipe(string nom) : nom(nom) {}

        void ajouter(Super * s) {
            equipe.push_back(s);
            nombre++;
        }  

        int getNombre() const {
            return nombre;
        }  

        int getNiveau() const {
            int somme = 0;
            for(auto& e: equipe) {
                somme += e->getNiveau();
            }
            return somme; 
        } 

       ~Equipe() {}  


}; 







#endif
